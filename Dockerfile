FROM python:3.8-alpine AS build

ARG S3QL_VERSION=5.1.2

COPY requirements.txt /
RUN apk --no-cache add curl signify g++ make pkgconfig fuse3-dev sqlite-dev libffi-dev
RUN pip install --user --ignore-installed -r requirements.txt
ARG FILE="s3ql-$S3QL_VERSION"
ARG URL="https://github.com/s3ql/s3ql/releases/download/s3ql-$S3QL_VERSION/$FILE.tar.gz"
RUN set -x; \
    curl -sfL "$URL" -o "/tmp/$FILE.tar.gz" \
 && curl -sfL "$URL.sig" -o "/tmp/$FILE.tar.gz.sig" \
 && tar xOf "/tmp/$FILE.tar.gz" "$FILE"/signify/s3ql-5.1.pub | signify -V -m "/tmp/$FILE.tar.gz" -p - \
 && tar -xmf "/tmp/$FILE.tar.gz"
WORKDIR $FILE
RUN python3 setup.py build_ext --inplace \
 && python3 setup.py install --user

FROM python:3.8-alpine
RUN apk --no-cache add fuse3 psmisc libstdc++
COPY --from=build /root/.local/bin/ /usr/local/bin/
COPY --from=build /root/.local/lib/ /usr/local/lib/
COPY ./authfile.sh ./entrypoint.sh ./mount.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/*.sh
ENV MOUNTPOINT=/s3ql
VOLUME /root/.s3ql
HEALTHCHECK CMD ["/bin/sh","-c","s3qlstat --quiet $MOUNTPOINT"]
ENTRYPOINT ["/bin/sh","/usr/local/bin/entrypoint.sh"]
CMD ["/bin/sh","/usr/local/bin/mount.sh"]
